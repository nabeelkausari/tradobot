import express from 'express';
import { createServer } from 'http';
import socketIO from 'socket.io';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import env from 'node-env-file';
import path from 'path';
import ccxt from 'ccxt';

import core from './core/index';
import setupDb from './db';

const prod = process.env.NODE_ENV === "production"
if (prod) {
  env('./conf.ini');
} else {
  env('./conf.dev.ini');
}

if (env('./.env')) {
  env('./.env')
}

const app = express();
const port = parseInt(process.env.PORT, 10) || 5000
const server = createServer(app)

app.use(bodyParser.json());
// app.use(morgan('dev'));

const io = socketIO(server)

setupDb();
core(ccxt, app, io);

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
  next();
});

app.use(express.static(path.join(__dirname + '/../client/build')));

app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname + '/../client/build/index.html'));
});

server.listen(port, err => {
  if (err) return console.log(err);
  console.log(`server listening on port ${port}`)
})
