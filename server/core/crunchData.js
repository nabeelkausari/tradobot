import { cloneDeep, map, uniq, flatten, remove } from 'lodash';
import groupData from './groupData';

export const getOpportunities = (stream, exchange, steps) => {
  let matches = []

  // Loop through all markets excluding the starting point
  for (let step of steps) {
    let paths = steps.filter(a => a !== step)

    for (let i=0; i < paths.length; i++) {
      let pMatches = getArbitrageSets(stream, step, paths[i], exchange);
      matches = matches.concat(pMatches);
    }
  }

  if(!matches.length) return matches;

  return matches
    .filter(m => Number(m.percentage) > 0)
    .sort((a, b) => parseFloat(b.rate) - parseFloat(a.rate))

}

const getArbitrageSets = (data, start, bPair, exchange) => {
  let keys = {
    a: start.toUpperCase(),
    b: bPair.toUpperCase(),
    c: 'findMe'.toUpperCase(),
  };

  let aPairs = cloneDeep(data.markets[keys.a]);
  let bPairs = cloneDeep(data.markets[keys.b]);

  let aKeys = [];
  aPairs.map(obj => { aKeys[obj.symbol.replace(keys.a, '').replace('/', '')] = obj; } );

  // prevent 1-steps
  delete aKeys[keys.b];

  /*
    Loop through BPairs
    for each bPair key, check if aPair has it too.
    If it does, run arbitrage math
  */

  let bMatches = [];
  for (let ib = 0; ib < bPairs.length; ib++) {
    let bPairTicker = bPairs[ib];
    bPairTicker.key = bPairTicker.symbol.replace(keys.b,'').replace('/', '');

    if(aKeys[bPairTicker.key]){
      keys.c = bPairTicker.key;
      let comparison = getArbitrageRate(data, keys.a, keys.b, keys.c, exchange);
      if (comparison) bMatches.push(comparison);
    }
  }

  return bMatches;
}

const getArbitrageRate = (stream, step1, step2, step3, exchange) => {
  if(!stream || !step1 || !step2 || !step3) return;
  let ret = {
    a: setAskBidPrices(stream, step1, step2),
    b: setAskBidPrices(stream, step2, step3),
    c: setAskBidPrices(stream, step3, step1)
  };

  if(!ret.a || !ret.b || !ret.c) return;

  ret.uniqueSteps = step1 + step2 + step3;
  ret.symbols = [ret.a.symbol, ret.b.symbol, ret.c.symbol];

  ret.rate = calculateRate(ret, exchange);
  return ret;
};

const setAskBidPrices = (stream, fromCur, toCur) => {
  /*
   CCXT uses xxx/BTC notation. If we're looking at xxx/BTC and we want to go from BTC to xxx,
   that means we're buying, vice versa for selling.
  */
  if(!stream || !fromCur || !toCur) return;

  let currency = cloneDeep(stream.raw[fromCur + '/' + toCur]);
  if(currency){
    currency.side = 'SELL';
    currency.rate = currency.bid;
  } else {
    currency = cloneDeep(stream.raw[toCur + '/' + fromCur]);
    if(!currency){
      // debugger;
      return false;
    }
    currency.side = 'BUY';
    currency.rate = currency.ask;
  }
  currency.stepFrom = fromCur;
  currency.stepTo = toCur;

  return currency;
}

export const getSymbols = (data, ex) => {
  let urls = [];
  let symbols = uniq(flatten(map(data, 'symbols')));
  for(let symbol of symbols) {
    urls.push(ex.fetchOrderBook(symbol, 5))
  }
  return { symbols, urls };
}

export const calculateRate = (ret, exchange) => {
  ret.a.startingBalance = getBalance(ret.a, exchange);
  let fee = exchange.fees.trading.taker;
  ret.a.trade = makeTrade(ret.a, ret.a.startingBalance, fee);
  ret.b.trade = makeTrade(ret.b, ret.a.trade, fee);
  ret.c.trade = makeTrade(ret.c, ret.b.trade, fee);

  ret.a.minTrade = checkMinTrade(exchange, ret.a)
  ret.b.minTrade = checkMinTrade(exchange, ret.b)
  ret.c.minTrade = checkMinTrade(exchange, ret.c)
  if (!ret.a.minTrade || !ret.b.minTrade || !ret.c.minTrade) {
    ret.minTradeError = true;
  }

  ret.percentage = ((ret.c.trade / ret.a.startingBalance) * 100) - 100;
  return ret.c.trade
}

const checkMinTrade = (exchange, start) => {
  let minTrade = start.side === "SELL" ? start.trade / start.bid : start.trade;
  return minTrade > exchange.markets[start.symbol].limits.amount.min;
}

export const makeTrade = (order, balance, fee) => {
  let trade = getTrade(order, balance)
  // let calculatedFee = (trade * fee) / 100;
  let calculatedFee = trade * fee;
  return trade - calculatedFee
}

const getTrade = (order, balance) => {
  if (!order.multi) {
    return order.side === 'BUY' ? (balance/order.rate) : balance * order.rate;
  }
  let trade = '';
  if (order.side === 'BUY') {
    trade = order.multiSteps[0][1] / order.multiSteps[0][0];
    trade += order.multiSteps[1][1] / order.multiSteps[1][0];
    if (order.multiSteps[2]) {
      trade += order.multiSteps[2][1] / order.multiSteps[2][0];
    }
  } else {
    trade = order.multiSteps[0][1] * order.multiSteps[0][0];
    trade += order.multiSteps[1][1] * order.multiSteps[1][0];
    if (order.multiSteps[2]) {
      trade += order.multiSteps[2][1] * order.multiSteps[2][0];
    }
  }
  return trade;
}

export const updateData = (data, newData, exchange, removeInsufficient) => {
  let toRemove = [];
  for (let i = 0; i < data.length; i++) {
    resetData(data[i].a, newData[data[i].a.symbol], getBalance(data[i].a, exchange))
    resetData(data[i].b, newData[data[i].b.symbol], data[i].a.trade)
    resetData(data[i].c, newData[data[i].c.symbol], data[i].b.trade)
    delete data[i].incompleteTrade;
    if (data[i].a.incompleteTrade || data[i].b.incompleteTrade || data[i].c.incompleteTrade) {
      data[i].incompleteTrade = true;
      toRemove.push(i)
    }
    data[i].rate = calculateRate(data[i], exchange)
  }
  if (removeInsufficient) {
    for (let i = toRemove.length -1; i >= 0; i--)
      data.splice(toRemove[i],1);
  }
  return data;
}

const resetData = (data, newData, volume) => {
  let side = data.side === 'SELL' ? 'bids' : 'asks';
  data.multi = newData[side][0][1] < volume
  data.rate = newData[side][0][0];
  data[side] = newData[side].slice(0, 3);
  if (data.multi) {
    makeMultiStepTrade(data, newData, volume, side)
  }
}

const makeMultiStepTrade = (data, newData, volume, side) => {
  data.volumeFilled = newData[side][0][1]
  data.multiSteps = [newData[side][0]]
  data.rate += newData[side][1][0]
  data.volumeFilled += newData[side][1][1]
  if (data.volumeFilled > volume) {
    data.multiSteps.push([newData[side][1][0], volume - newData[side][0][1]])
  } else {
    data.multiSteps.push(newData[side][1]);

    data.rate += newData[side][2][0]
    data.volumeFilled += newData[side][2][1]

    if (data.volumeFilled > volume) {
      data.multiSteps.push([newData[side][2][0], volume - (newData[side][0][1] + newData[side][1][1])])
    } else {
      data.multiSteps.push(newData[side][2]);
      data.incompleteTrade = true;
    }
  }

}

function getBalance(start, exchange) {
  const minTrades = {
    'USDT': 30,
    'BTC': 0.003,
    'ETH': 0.05,
    'LTC': 0.2,
    'NEO': 0.4,
    'BNB': 1,
    'USD': 30,
    'EUR': 8,
    'WEUR': 9,
    'WUSD': 12,
    'WAVES': 2
  }
  return start.side === 'BUY'
    ? minTrades[exchange.markets[start.symbol].quoteId]
    : minTrades[exchange.markets[start.symbol].baseId];
}

export const streamCall = async (exchange, marketsData) => {
  console.log('interval: ', new Date())
  let markets = marketsData.map(m => m.symbol);
  let steps = getSteps(markets);
  let tickers = await exchange.fetchTickers(markets);
  let groupedData = groupData(tickers, steps);
  let freshData = getOpportunities(groupedData, exchange, steps);

  let { symbols, urls } = getSymbols(freshData, exchange);
  let resolved = await Promise.all(urls);
  let data = {};
  for (let i = 0; i < symbols.length; i++) {
    data[symbols[i]] = resolved[i]
  }

  console.log('fresh stream going on', JSON.stringify(symbols))
  return updateData(freshData, data, exchange, true)
}

export const getSteps = (markets) => {
  return uniq(markets.map(market => market.split('/')[1]));
}
