import fetch from 'node-fetch';
import { uniq, difference, concat, without } from 'lodash';

import TickerSchema from './models/Ticker';

export default async (io, duration) => {
  let ticker = async () => {
    try {
      let urls = [
        'https://bitbns.com/order/getTickerAll',
        'https://coindelta.com/api/v1/public/getticker',
        'https://koinex.in/api/ticker',
        'https://www.zebapi.com/api/v1/market',
        'https://api.wazirx.com/api/v2/tickers'
      ];

      let [bitBns, coinDelta, koinex, zebpay, wazirx] = await Promise.all(
        urls.map(url => fetch(url, {cache: "no-cache"})
          .then(res => res.json())
          .catch(() => {return {error: true}})
        )
      )

      let bitBnsData = bitBns.map(d => ({
        key: Object.keys(d)[0],
        buy: d[Object.keys(d)[0]].buyPrice,
        sell: d[Object.keys(d)[0]].sellPrice,
        buyFee: 0.25,
        sellFee: 0.25
      }));

      let coinDeltaData = coinDelta
        .filter(d => d.MarketName.split('-')[1] === 'inr')
        .map(d => ({
          key: d.MarketName.split('-')[0].toUpperCase(),
          buy: d.Bid,
          sell: d.Ask,
          buyFee: 0.3,
          sellFee: 0.3
        }));

      let koinexData = koinex.stats ? Object.values(koinex.stats).map(d => ({
        key: d.currency_short_form,
        buy: Number(d.highest_bid),
        sell: Number(d.lowest_ask),
        buyFee: 0,
        sellFee: 0.2
      })) : []

      let zebpayData = zebpay
        .filter(d => d.pair.split('-')[1] === 'INR')
        .map(d => ({
          key: d.pair.split('-')[0],
          buy: Number(d.sell),
          sell: Number(d.buy),
          buyFee: 0.25,
          sellFee: 0.25
        }))

      let wazirxData = Object.values(wazirx).map(d => ({
        key: d.name.split('/')[0],
        buy: Number(d.buy),
        sell: Number(d.sell),
        buyFee: 0.25,
        sellFee: 0.25
      }))

      let data = [{
        exchange: 'BitBNS',
        data: bitBnsData,
      }, {
        exchange: 'Koinex',
        data: koinexData
      }, {
        exchange: 'WazirX',
        data: wazirxData
      }, {
        exchange: 'Zebpay',
        data: zebpayData
      }, {
        exchange: 'CoinDelta',
        data: coinDeltaData
      }]

      data = data.map(d => {
        let others = data.filter(dt => dt.exchange !== d.exchange);
        return others.map(o => [d, o]);
      })

      console.log(new Date())
      console.log(data)

      // let CEXEURdata = {};
      // let CEXUSDdata = {};
      // let BNSdata = {};
      // let coinDeltaData = {};
      // let koinexData = koinex && koinex.stats ? koinex.stats : {};
      //
      // if (!foreignExchangeDataEUR.error) {
      //   for (let item of foreignExchangeDataEUR.data) {
      //     CEXEURdata[item.pair.split(':')[0]] = item;
      //   }
      // }
      //
      // if (!foreignExchangeDataUSD.error) {
      //   for (let item of foreignExchangeDataUSD.data) {
      //     CEXUSDdata[item.pair.split(':')[0]] = item;
      //   }
      // }
      //
      // if (!localExchangeData.error) {
      //   for (let item of localExchangeData) {
      //     BNSdata[Object.keys(item)[0]] = item[Object.keys(item)[0]]
      //   }
      // }
      //
      // if (!coinDelta.error) {
      //   for (let item of coinDelta) {
      //     let markets = item.MarketName.split('-')
      //     if (markets[1] === 'inr') {
      //       coinDeltaData[markets[0].toUpperCase()] = item
      //     }
      //   }
      // }
      //
      // let uniqs = uniq(concat(Object.keys(CEXEURdata), Object.keys(BNSdata), Object.keys(coinDeltaData), Object.keys(koinexData)))
      // let uniqsIN = uniq(concat(Object.keys(BNSdata), Object.keys(koinexData)))
      //
      // let ret = {};
      //
      // for (let item of uniqsIN) {
      //   if (BNSdata[item] && koinexData[item]) {
      //     ret[item] = {
      //       koinexSELL: koinexData[item].last_traded_price,
      //       koinexBUY: koinexData[item].last_traded_price + (koinexData[item].last_traded_price * 0.002) ,
      //       bitbnsSELL: BNSdata[item].buyPrice - (BNSdata[item].buyPrice * 0.0025),
      //       bitbnsBUY: BNSdata[item].sellPrice + (BNSdata[item].sellPrice * 0.0025)
      //     }
      //   }
      // }

      // if (Object.keys(ret).length > 0) {
      io.emit('tickerEmit', { data, _created: new Date() })
      //   // let ticker = new TickerSchema({ data: ret })
      //   // ticker.save((err, doc) => {
      //   //   io.emit('tickerEmit', doc)
      //   // });
      // } else {
      //   console.log('NO DATA')
      // }
    } catch (err) {
      console.log(err);
      io.emit('tickerEmit', { error: true, log: err })
    }
  }
  ticker();
  return setInterval(ticker, duration)
}
