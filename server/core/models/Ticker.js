import mongoose, { Schema } from 'mongoose'

const TickerSchema = new Schema({
  data: { type: Object }
},{
  timestamps:  { createdAt: '_created', updatedAt: '_updated' },
  versionKey: false
});

export default mongoose.model('Ticker', TickerSchema);
