import mongoose, { Schema } from 'mongoose'

const BittrexSchema = new Schema({
  data: { type: Object }
},{
  timestamps:  { createdAt: '_created' },
  versionKey: false
});

export default mongoose.model('Bittrex', BittrexSchema);
