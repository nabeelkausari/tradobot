export default (data, steps) => {
  const output = {
    raw: data,
    markets: [] // Sub objects with only data on specific markets
  };
  const values = Object.keys(data).map(key => data[key]);

  for (let i=0; i< steps.length; i++) {
    output.markets[steps[i]] = values.filter(item => {
      return (item.symbol.endsWith(steps[i]) || item.symbol.startsWith(steps[i]))
    });
  }

  return output;
}
