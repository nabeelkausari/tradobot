import fetch from 'node-fetch';
import { uniq, difference, concat, without } from 'lodash';

import TickerSchema from './models/Ticker';

export default async (io, usdValue, eurValue, duration) => {
  return setInterval(async () => {
    try {
      let foreignExchangeDataEUR = await fetch('https://cex.io/api/tickers/XRP/EUR').then(res => res.json()).catch(() => {return {error: true}});
      let foreignExchangeDataUSD = await fetch('https://cex.io/api/tickers/XRP/USD').then(res => res.json()).catch(() => {return {error: true}})
      let localExchangeData = await fetch('https://bitbns.com/order/getTickerAll').then(res => res.json()).catch(() => {return {error: true}})
      let coinDelta = await fetch('https://coindelta.com/api/v1/public/getticker').then(res => res.json()).catch(() => {return {error: true}})
      let koinex = await fetch('https://koinex.in/api/ticker').then(res => res.json()).catch(() => {return {error: true}})
      let CEXEURdata = {};
      let CEXUSDdata = {};
      let BNSdata = {};
      let coinDeltaData = {};
      let koinexData = koinex && koinex.stats ? koinex.stats : {};

      if (!foreignExchangeDataEUR.error) {
        for (let item of foreignExchangeDataEUR.data) {
          CEXEURdata[item.pair.split(':')[0]] = item;
        }
      }

      if (!foreignExchangeDataUSD.error) {
        for (let item of foreignExchangeDataUSD.data) {
          CEXUSDdata[item.pair.split(':')[0]] = item;
        }
      }

      if (!localExchangeData.error) {
        for (let item of localExchangeData) {
          BNSdata[Object.keys(item)[0]] = item[Object.keys(item)[0]]
        }
      }

      if (!coinDelta.error) {
        for (let item of coinDelta) {
          let markets = item.MarketName.split('-')
          if (markets[1] === 'inr') {
            coinDeltaData[markets[0].toUpperCase()] = item
          }
        }
      }

      let uniqs = uniq(concat(Object.keys(CEXEURdata), Object.keys(BNSdata), Object.keys(coinDeltaData), Object.keys(koinexData)))
      let uniqsIN = uniq(concat(Object.keys(BNSdata), Object.keys(koinexData)))

      let ret = {};

      for (let item of uniqsIN) {
        if (BNSdata[item] && koinexData[item]) {
          ret[item] = {
            koinexSELL: koinexData[item].last_traded_price,
            koinexBUY: koinexData[item].last_traded_price + (koinexData[item].last_traded_price * 0.002) ,
            bitbnsSELL: BNSdata[item].buyPrice - (BNSdata[item].buyPrice * 0.0025),
            bitbnsBUY: BNSdata[item].sellPrice + (BNSdata[item].sellPrice * 0.0025)
          }
        }
      }

      // for (let item of uniqs) {
      //   if (CEXUSDdata[item] && BNSdata[item]) {
      //     ret[item] = {
      //       cexUSDASK: Number((CEXUSDdata[item].ask * (usdValue || 70)).toFixed(2)),
      //       cexUSDBID: Number((CEXUSDdata[item].bid * 63).toFixed(2)),
      //       cexEURASK: Number((CEXEURdata[item].ask * (eurValue || 87)).toFixed(2)),
      //       cexEURBID: Number((CEXEURdata[item].bid * 77.5).toFixed(2)),
      //       bitbnsSELL: BNSdata[item].buyPrice,
      //       bitbnsBUY: BNSdata[item].sellPrice
      //     }
      //   }
      //   if (CEXUSDdata[item] && coinDeltaData[item]) {
      //     ret[item] = {
      //       ...ret[item],
      //       cexUSDASK: Number((CEXUSDdata[item].ask * (usdValue || 70)).toFixed(2)),
      //       cexUSDBID: Number((CEXUSDdata[item].bid * 63).toFixed(2)),
      //       cexEURASK: Number((CEXEURdata[item].ask * (eurValue || 87)).toFixed(2)),
      //       cexEURBID: Number((CEXEURdata[item].bid * 77.5).toFixed(2)),
      //       coinDeltaAsk: coinDeltaData[item].Ask,
      //       coinDeltaBid: coinDeltaData[item].Bid
      //     }
      //   }
      //   if (CEXUSDdata[item] && koinexData[item]) {
      //     ret[item] = {
      //       ...ret[item],
      //       cexUSDASK: Number((CEXUSDdata[item].ask * (usdValue || 70)).toFixed(2)),
      //       cexUSDBID: Number((CEXUSDdata[item].bid * 63).toFixed(2)),
      //       cexEURASK: Number((CEXEURdata[item].ask * (eurValue || 87)).toFixed(2)),
      //       cexEURBID: Number((CEXEURdata[item].bid * 77.5).toFixed(2)),
      //       koinex: koinexData[item].last_traded_price,
      //     }
      //   }
      // }
      if (Object.keys(ret).length > 0) {
        io.emit('tickerEmit', { data: ret, _created: new Date() })
        // let ticker = new TickerSchema({ data: ret })
        // ticker.save((err, doc) => {
        //   io.emit('tickerEmit', doc)
        // });
      } else {
        console.log('NO DATA')
      }
    } catch (err) {
      console.log(err);
      io.emit('tickerEmit', { error: true, log: err })
    }
  }, duration)
}
