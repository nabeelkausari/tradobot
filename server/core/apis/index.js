export default (ccxt, app, io) => {
  app.get('/api/exchangeDetails', async ({ query: { exchange } }, res) => {
    try {
      let ex = setupExchange(ccxt, exchange);
      res.json(ex)
    } catch (e) {
      console.log(e)
      res.json({ error: e })
    }
  })

  app.get('/api/balance', async ({ query: { exchange } }, res) => {
    try {
      let ex = setupExchange(ccxt, exchange);
      let balance = await ex.fetchBalance()
      res.json(balance)
    } catch (e) {
      console.log(e)
      res.json({ error: e })
    }
  })

  app.get('/api/fetchOrders', async ({ query: { exchange, symbol } }, res) => {
    try {
      let ex = setupExchange(ccxt, exchange);
      res.json(await ex.fetchOrders(symbol))
    } catch (e) {
      console.log(e)
      res.json({ error: e })
    }
  })

  app.get('/api/fetchOpenOrders', async ({ query: { exchange, symbol } }, res) => {
    try {
      let ex = setupExchange(ccxt, exchange);
      res.json(await ex.fetchOpenOrders(symbol))
    } catch (e) {
      console.log(e)
      res.json({ error: e })
    }
  })

  app.get('/api/fetchClosedOrders', async ({ query: { exchange, symbol } }, res) => {
    try {
      let ex = setupExchange(ccxt, exchange);
      let orders = await ex.fetchClosedOrders(symbol)
      res.json(orders)
    } catch (e) {
      console.log(e)
      res.json({ error: e })
    }
  })

  app.get('/api/fetchMarkets', async ({ query: { exchange } }, res) => {
    try {
      let ex = setupExchange(ccxt, exchange);
      res.json(await ex.fetchMarkets())
    } catch (e) {
      console.log(e)
      res.json({ error: e })
    }
  })

  app.post('/api/createOrder', async (req, res) => {
    try {
      let { exchange, symbol, type, side, amount, price } = req.body;
      let ex = setupExchange(ccxt, exchange);
      res.json(await ex.createOrder(symbol, type, side, amount, price))
    } catch (e) {
      console.log(e)
      res.json({ error: e })
    }
  })

  app.post('/api/cancelOrder', async (req, res) => {
    try {
      let { exchange, id } = req.body;
      let ex = setupExchange(ccxt, exchange);
      res.json(await ex.cancelOrder(id))
    } catch (e) {
      console.log(e)
      res.json({ error: e })
    }
  })

  app.post('/api/fetchOrder', async (req, res) => {
    try {
      let { exchange, id } = req.body;
      let ex = setupExchange(ccxt, exchange);
      res.json(await ex.fetchOrder(id))
    } catch (e) {
      console.log(e)
      res.json({ error: e })
    }
  })
}

function setupExchange(ccxt, exchange) {
  return new ccxt[exchange]({
    apiKey: process.env[`${exchange.toUpperCase()}_KEY`],
    secret: process.env[`${exchange.toUpperCase()}_SECRET`]
  });
}
