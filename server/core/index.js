import { getSymbols, getSteps, streamCall, updateData} from './crunchData';
import tickerSocket from './tickerSockets'
import Bittrex from './models/Bittrex';
import apis from './apis/index';

export default (ccxt, app, io) => {
  let streams = [];
  tickerSocket(io, 5000).then(() => console.log('ticker started'))

  let clearStreams = () => {
    if (streams.length > 0) {
      console.log('streams.length: ', streams.length)
      for (let stream of streams) {
        let now = new Date();
        // clear streams greater than 30 minutes
        if (((now - stream.time) / 1000) > 1800) {
          clearInterval(stream.stream)
        }
      }
    }
  }


  setInterval(() => clearStreams(), 10000)

  apis(ccxt, app, io)

  app.get('/api/watchList', async ({ query: { exchange, marketsData } }, res) => {
    if (!exchange || !marketsData) {
      res.json({ error: 'Incomplete request parameters'})
    }
    try {
      let ex = new ccxt[exchange]();

      let { symbols, urls } = getSymbols(JSON.parse(marketsData), ex);
      let resolved = await Promise.all(urls);
      let data = {};
      for (let i = 0; i < symbols.length; i++) {
        data[symbols[i]] = resolved[i]
      }
      let newData = updateData(JSON.parse(marketsData), data, ex);
      res.json({ data: newData, updatedAt: new Date()})
    } catch (e) {
      console.log(e)
      res.json({ error: true })
    }
  })

  app.get('/api/opportunities', async ({ query: { exchange } }, res) => {
    if (!exchange) {
      res.json({ error: 'Incomplete request parameters'})
    }
    try {
      let response = await Bittrex.findOne().sort('-_created').exec();
      res.json(response.data.sort((a, b) => a.percentage < b.percentage))
    } catch (e) {
      console.log(e)
    }

  });

  async function startStream(exchange, className) {
    try {
      let ex = new ccxt[exchange]();
      let marketsData = await ex.fetchMarkets();

      streams.push(setInterval(async () => {
        let data = await streamCall(ex, marketsData)
        await new className({ data }).save()
      }, 15000))

    } catch (e) {
      console.log(e)
    }
  }

  startStream('bittrex', Bittrex)
    .then(() => console.log('stream bittrex started 1'))

  setTimeout(() => {
    startStream('bittrex', Bittrex)
      .then(() => console.log('stream bittrex started 2'))
  }, 5000)

  setTimeout(() => {
    startStream('bittrex', Bittrex)
      .then(() => console.log('stream bittrex started 3'))
  }, 10000)
}
