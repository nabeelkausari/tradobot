import React, { Component } from 'react';
import { Table } from 'reactstrap';
import MarketItem from "./MarketItem"

export default props => {
  const { marketData, noHeader, list } = props;
  if (!marketData.length) return <h4>Loading...</h4>;
  if (list.length > 0) {
    let toRemove = []
    marketData.forEach((item, i) => {
      if (list.indexOf(item.uniqueSteps) >= 0) {
        toRemove.push(i)
      }
    })
    for (let i = toRemove.length -1; i >= 0; i--)
      marketData.splice(toRemove[i],1);
  }
  if (!marketData.length) return <h4>No Opportunities!</h4>;
  return (
    <div>
      <h4>Opportunities</h4>
      <Table>
        {!noHeader && <thead>
        <tr>
          <th>Step A</th>
          <th> </th>
          <th>Step B</th>
          <th> </th>
          <th>Step C</th>
          <th>Profit - Fees</th>
          <th>Watch</th>
        </tr>
        </thead>}

        <tbody>
        {marketData.map(market => <MarketRow {...props} key={market.uniqueSteps} market={market}/>)}
        </tbody>
      </Table>
    </div>
  )
}

class MarketRow extends Component {
  state = {
    orderBook: false
  }

  toggleOrderBook = () => this.setState({ orderBook: !this.state.orderBook })


  render() {
    const { market } = this.props;
    const { orderBook } = this.state;
    return (
      <tr>
        <td
          title={`Start: ${market.a.startingBalance} ${market.a.stepFrom}`}
          style={{cursor: 'pointer'}} onClick={this.toggleOrderBook}>
          <MarketItem orderBook={orderBook} market={market.a} uniqueSteps={market.uniqueSteps}/></td>
        <td> -> </td>
        <td style={{cursor: 'pointer'}} onClick={this.toggleOrderBook}><MarketItem orderBook={orderBook} market={market.b} uniqueSteps={market.uniqueSteps}/></td>
        <td> -> </td>
        <td style={{cursor: 'pointer'}} onClick={this.toggleOrderBook}><MarketItem orderBook={orderBook} market={market.c} uniqueSteps={market.uniqueSteps}/></td>
        <td style={{ color: getColor(market.percentage)}}>{market.percentage.toFixed(3)}%</td>
        <td><button className="btn btn-primary" onClick={() => this.props.add(market)}>+</button></td>
      </tr>
    )
  }
}


function getColor(rate) {
  return rate > 0 ? 'green' : 'red'
}
