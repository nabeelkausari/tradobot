import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Table } from 'reactstrap';
import OrderItem from "./OrderItem"
import { execute } from '../store/actions/orders';

class ExecuteOrder extends Component {

  handleExecute = () => {
    const { a, b, c } = this.props.market;
    const execProps = {
      exchange: "bittrex",
      orderA: {
        type: "limit",
        symbol: a.symbol,
        side: a.side.toLowerCase(),
        amount: a.side === "SELL" ? a.startingBalance : a.trade.toFixed(8),
        price: a.bids ? a.bids[0][0] : a.asks[0][0]
      },
      orderB: {
        type: "limit",
        symbol: b.symbol,
        side: b.side.toLowerCase(),
        amount: b.side === "SELL" ? a.trade.toFixed(8) : b.trade.toFixed(8),
        price: b.bids ? b.bids[0][0] : b.asks[0][0]
      },
      orderC: {
        type: "limit",
        symbol: c.symbol,
        side: c.side.toLowerCase(),
        amount: c.side === "SELL" ? b.trade.toFixed(8) : c.trade.toFixed(8),
        price: c.bids ? c.bids[0][0] : c.asks[0][0]
      }
    }
    console.log(execProps)
    this.props.execute(execProps)
  }

  render() {
    const { market, orderAProcessing, orderACompleted, orderBProcessing, orderBCompleted, orderCProcessing, orderCCompleted, balances } = this.props;
    if (!market) return <div/>
    return (
      <div className="execute">
        <h4>Execution</h4>
        <Table>
          <tbody>
          <tr>
            <td><OrderItem
              processing={orderAProcessing}
              completed={orderACompleted}
              marketItem={market.a}
              uniqueSteps={market.uniqueSteps}
              balances={balances}
            /></td>
            <td> -> </td>
            <td><OrderItem
              processing={orderBProcessing}
              completed={orderBCompleted}
              marketItem={market.b}
              uniqueSteps={market.uniqueSteps}
              balances={balances}
            /></td>
            <td> -> </td>
            <td><OrderItem
              processing={orderCProcessing}
              completed={orderCCompleted}
              marketItem={market.c}
              uniqueSteps={market.uniqueSteps}
              balances={balances}
            /></td>
            <td style={{ color: getColor(market.percentage)}}>{market.percentage.toFixed(3)}%</td>
            <td><button onClick={this.handleExecute} className="btn btn-primary">Execute</button></td>
          </tr>
          </tbody>
        </Table>
      </div>
    )
  }
}

function getColor(rate) {
  return rate > 0 ? 'green' : 'red'
}

function mapStateToProps(state) {
  return {
    ...state.orders
  }
}

export default connect(mapStateToProps, { execute })(ExecuteOrder)
