import React, { Component } from 'react';
import { uniq, concat, without } from 'lodash';
import { Row, Col, Table, Badge } from 'reactstrap';
import io from 'socket.io-client';
import Select from 'react-select';

const socket = io();


class Arbi extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      currentTime: '',
      timer: null,
      defaultCurrencies: [
        { label: 'ETH', value: 'ETH' },
        { label: 'XRP', value: 'XRP' },
      ],
      currencies: 'ETH,XRP'
    };

    socket.on('tickerEmit', data => {
      console.log(data)
      let defaultCurrencies = getCurrencies(data.data[0])
      this.setState({ defaultCurrencies })
      let filteredData = [];
      data.data.forEach(a => {
        let aSet = [];

        a.forEach(b => {
          let bSet = [];

          b.forEach(c => {
            let selected = [];
            let currencies = this.state.currencies || 'ETH,XRP';

            currencies.split(',').forEach(curr => {
              selected.push(c.data.filter(d => d.key === curr)[0])
            });

            bSet.push({ exchange: c.exchange, data: selected })
          });

          aSet.push(bSet)
        })
        filteredData.push(aSet)
      })
      this.setState({ data: { data: filteredData, _created: data._created } })
    });
  }

  startTime() {
    this.setState({ currentTime: new Date().toLocaleTimeString() })
  }

  componentWillMount() {
    this.state.timer = setInterval(this.startTime.bind(this), 1000)
  }

  componentWillUnmount() {
    clearInterval(this.state.timer)
  }

  handleSelectChange = currencies => {
    console.log('You\'ve selected:', currencies);
    this.setState({ currencies });
  }

  render() {
    let { data: { data, _created }, currentTime, defaultCurrencies } = this.state;
    if (!data) return <div>Loading....</div>
    return (
      <div>
        <h6>Last Updated: <Badge color="success">{ new Date(_created).toLocaleTimeString() }</Badge> | Current Time: <Badge color="warning">{ currentTime }</Badge></h6>
        <Row>
          <Col md={3}/>
          <Col md={6} style={{ textAlign: 'left' }}>
            <Select
              closeOnSelect
              multi
              onChange={this.handleSelectChange}
              options={defaultCurrencies}
              placeholder="Select Currencies"
              removeSelected
              simpleValue
              value={this.state.currencies}
              style={{
                margin: '30px auto',
                textAlign: 'left'
              }}
            />
          </Col>
        </Row>
        <Row>
          <Col md={1}>
            {data.map((d, di) => (
              <Row key={di}>
                <Col md={12}>
                  <h6>Coin</h6>
                  <Table striped style={{ fontSize: 12, lineHeight: 1.2 }}>
                    <thead>
                    <tr>
                      <td>Currency</td>
                    </tr>
                    </thead>
                    <tbody>
                    { d[0][0].data.map(item => {
                      if (item) return <tr key={item.key}>
                        <td><b>{item.key}</b></td>
                      </tr>
                    }) }
                    </tbody>
                  </Table>
                </Col>
              </Row>
            ))}
          </Col>
          <Col md={11}>
            {data.map((d, di) => (
              <Row key={di}>
                {d.map((set, i) => (
                  <Col key={i} md={3}>
                    <h6>{set[0].exchange} -> {set[1].exchange}</h6>
                    <Table striped style={{ fontSize: 12, lineHeight: 1.2 }}>
                      <thead>
                      <tr>
                        <td>Buy</td>
                        <td>Sell</td>
                        <td>Profit</td>
                      </tr>
                      </thead>
                      <tbody>
                      { set[0].data.map(item => {
                        if (item) return <tr key={item.key}>
                          <td>{item.sell}</td>
                          <td>{sellItem(item, set[1]) ? sellItem(item, set[1]).buy : '-'}</td>
                          { sellItem(item, set[1]) ? <td style={{ color: getColor(getPercentage(item, sellItem(item, set[1])))}}>
                            {getPercentage(item, sellItem(item, set[1]))}%
                          </td> : <td>-</td>}
                        </tr>
                      }) }
                      </tbody>
                    </Table>
                  </Col>
                ))}
              </Row>
            ))}
          </Col>
        </Row>

      </div>
    )
  }
}

function sellItem(first, second) {
  if (first) {
    return second.data.filter(s => s && s.key === first.key)[0]
  }
}

function getPercentage(first, second) {
  return (100 -
    (
      (
        (first.sell + (first.sell * (first.sellFee / 100))) /
        (second.buy - (second.buy * (second.buyFee / 100)))
        * 100)
  )).toFixed(2)
}

function getColor(rate) {
  return rate > 0 ? 'green' : 'red'
}

function getCurrencies(data) {
  let names = []
  data.forEach((a,i) => {
    if (i === 0) names.push(a[0].data.map(d => d.key));
    names.push(a[1].data.map(d => d.key));
  })

  return without(uniq(concat(...names)), undefined)
    .map(a => ({ label: a, value: a }))
}

export default Arbi;
