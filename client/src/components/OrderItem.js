import React, { Component }  from 'react';
import { Badge } from 'reactstrap';
import OrderBook from './OrderBook';

class OrderItem extends Component {

  render() {
    let { marketItem, processing, completed, balances } = this.props;
    const { symbol, side, stepFrom, stepTo, bids, asks, trade } = marketItem;
    return (
      <div>
        <h6>
          <span style={styles.bold}>{stepFrom}</span> ({symbol}) <Badge color={marketColor(side)}>{side}</Badge>
        </h6>
        {getTradeText(stepFrom, stepTo, trade, bids, asks)}
        {balances && <p style={{ fontSize: 12 }}>{`Bal: ${getBalanceText(symbol, balances, 0)} , ${getBalanceText(symbol, balances, 1)}`}</p>}
        <p>{ processing && 'Processing'}</p>
        <p>{ completed && 'Completed'}</p>
        <OrderBook bids={bids} asks={asks} symbol={symbol} />
      </div>
    )
  }

}

function getTradeText(stepFrom, stepTo, trade, bids, asks) {
  return <p style={{ fontSize: 12 }}>
    Exchange <b>{stepFrom}</b> for <b>{trade.toFixed(8)} {stepTo}</b> at price <b>{bids ? bids[0][0] : asks[0][0]} {stepFrom}</b>
    </p>
}

function getBalanceText(symbol, balances, index) {
  let count = balances[symbol.split('/')[index]] ? balances[symbol.split('/')[index]].free : 0;
  return `${count} ${symbol.split('/')[index]}`
}

const styles = {
  bold: {
    fontWeight: 'bold'
  },
  target: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#4b4e51',
    borderRadius: 5
  }
}

function marketColor(side) {
  return side === 'BUY' ? 'success' : 'danger'
}

export default OrderItem;
