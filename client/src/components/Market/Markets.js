import React, { Component } from 'react';
import {connect } from 'react-redux';
import { Button, Badge } from 'reactstrap';
import { Link } from 'react-router-dom';

import {getMarkets, updateMarkets, getWatchList, updateWatchList } from '../../store/actions/market'
import { getBalance, selectOrder } from '../../store/actions/orders'
import './market.css';
import MarketData from "../MarketData"
import WatchListData from "../WatchListData"
import ExchangeSelector from "../ExchangeSelector"
import ExecuteOrder from "../ExecuteOrder"

class Markets extends Component {

  constructor(props) {
    super(props)
    this.state = {
      previousSnapshot: [],
      snapshotTs: new Date(),
      exchange: 'Bittrex',
      startingBalance: 0.01,
      watchList: {},
      intervalId: null,
      marketIntervalId: null
    }
  }

  componentWillMount() {
    this.props.getBalance({ exchange: 'bittrex' });
    this.props.getMarkets(this.state);
    this.startMarketStream();
  }

  componentWillUnmount() {
    if (this.state.intervalId !== null) {
      clearInterval(this.state.intervalId)
    }
    clearInterval(this.state.marketIntervalId)
  }

  componentWillReceiveProps(nextProps) {
    let wl = {}
    let wlUpdated = nextProps.watchListMarkets
      && nextProps.watchListMarkets.updatedAt !== this.props.watchListMarkets.updatedAt;
    if (wlUpdated) {
      nextProps.watchListMarkets.data.forEach(item => {
        if (this.state.watchList[item.uniqueSteps]) {
          wl[item.uniqueSteps] = item
        }
      });
      this.setState({ watchList: { ...this.state.watchList, ...wl}})
    }
  }

  addWatchList = item => {
    this.props.selectOrder(item)
    let watchList = Object.keys(this.state.watchList).length > 0
      ? { ...this.state.watchList, [item.uniqueSteps] : item }
      : { [item.uniqueSteps] : item }
    this.setState({ watchList })
    this.startStream(watchList)
  }

  removeWatchList = key => {
    const {[key]:val, ...watchList} = this.state.watchList;
    this.setState({ watchList });
    this.startStream(watchList)
  }

  startStream = watchList => {
    if (this.state.intervalId !== null) {
      clearInterval(this.state.intervalId)
    }
    let intervalId = setInterval(() => {
      this.props.getWatchList({
        marketsData: Object.values(watchList),
        exchange: 'bittrex'
      });
    }, 1000)
    this.setState({ intervalId })
  }

  startMarketStream = () => {
    if (this.state.marketIntervalId !== null) {
      clearInterval(this.state.marketIntervalId)
    }
    let marketIntervalId = setInterval(() => {
      this.props.getMarkets(this.state);
    }, 3000)
    this.setState({ marketIntervalId })
  }

  changeExchange(exchange) {
    this.setState({ exchange });
    this.props.getMarkets({...this.state, exchange});
  }

  render() {
    let { exchange, watchList } = this.state;
    let { markets, orders } = this.props;
    if (markets.error) return <div><h3>Something went wrong</h3></div>

    return (
      <div>
        <h3 style={{ marginBottom: 20 }}>
          Top {markets.length} Arbitrage Opportunities within <ExchangeSelector changeExchange={this.changeExchange.bind(this)} selectedExchange={exchange}/> | <Badge color="warning">{ new Date().toLocaleTimeString() }</Badge></h3>
        <ExecuteOrder market={orders.selected}/>
        <WatchListData list={Object.values(watchList)} remove={this.removeWatchList} {...this.props} />
        <MarketData list={Object.keys(watchList)} add={this.addWatchList} {...this.props} marketData={markets} />
      </div>
    );
  }
}


const mapStateToProps = state => {
  const {markets, updatedMarkets, watchListMarkets, orders} = state
  return ({ markets, updatedMarkets, watchListMarkets, orders })
}

export default connect(mapStateToProps, { getBalance, getMarkets, updateMarkets, getWatchList, updateWatchList, selectOrder })(Markets);
