import React, { Component } from 'react';
import { Table } from 'reactstrap';
import MarketItem from "./MarketItem"

export default props => {
  const { noHeader, list } = props;
  if (!list.length) return <div/>;
  return (
    <div>
      <h4>Watch List</h4>
      <Table>
        {!noHeader && <thead>
        <tr>
          <th>Step A</th>
          <th> </th>
          <th>Step B</th>
          <th> </th>
          <th>Step C</th>
          <th>Profit - Fees</th>
          <th>Watch</th>
        </tr>
        </thead>}

        <tbody>
        {list.map(market => <MarketRow {...props} key={market.uniqueSteps} market={market}/>)}
        </tbody>
      </Table>
    </div>
  )
}

class MarketRow extends Component {
  state = {
    orderBook: true
  }

  toggleOrderBook = () => this.setState({ orderBook: !this.state.orderBook })

  render() {
    const { market } = this.props;
    const { orderBook } = this.state;
    return (
      <tr>
        <td style={{cursor: 'pointer'}} onClick={this.toggleOrderBook}><MarketItem orderBook={orderBook} market={market.a} uniqueSteps={market.uniqueSteps}/></td>
        <td> -> </td>
        <td style={{cursor: 'pointer'}} onClick={this.toggleOrderBook}><MarketItem orderBook={orderBook} market={market.b} uniqueSteps={market.uniqueSteps}/></td>
        <td> -> </td>
        <td style={{cursor: 'pointer'}} onClick={this.toggleOrderBook}><MarketItem orderBook={orderBook} market={market.c} uniqueSteps={market.uniqueSteps}/></td>
        <td style={{ color: getColor(market.percentage)}}>{market.percentage.toFixed(3)}%</td>
        <td><button className="btn btn-warning" onClick={() => this.props.remove(market.uniqueSteps)}>-</button></td>
      </tr>
    )
  }
}


function getColor(rate) {
  return rate > 0 ? 'green' : 'red'
}
