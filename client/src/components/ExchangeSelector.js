import React from 'react';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

export default class ExchangeSelector extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropDownOpen: false
    };
  }

  toggle() {
    this.setState({
      dropDownOpen: !this.state.dropDownOpen
    });
  }

  render() {
    let { changeExchange, selectedExchange } = this.props;
    return (
      <ButtonDropdown
        isOpen={this.state.dropDownOpen}
        toggle={this.toggle}>
        <DropdownToggle caret>
          {selectedExchange ? selectedExchange : 'Select Exchange'}
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem onClick={() => changeExchange('Binance')}>Binance</DropdownItem>
          <DropdownItem onClick={() => changeExchange('Bittrex')}>Bittrex</DropdownItem>
          <DropdownItem onClick={() => changeExchange('bitfinex2')}>Bitfinex</DropdownItem>
          <DropdownItem onClick={() => changeExchange('Tidex')}>Tidex</DropdownItem>
        </DropdownMenu>
      </ButtonDropdown>
    );
  }
}

const styles = {
  input: {
    display: 'inline-flex',
    lineHeight: 1.6,
    verticalAlign: 'middle',
    width: 60,
    marginLeft: 10,
    marginRight: 10,
  }
}
