import React, { Component }  from 'react';
import { Badge } from 'reactstrap';

import OrderBook from './OrderBook';

class MarketItem extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      popoverOpen: false
    };
  }

  toggle() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    });
  }

  render() {
    let { market: { symbol, side, stepFrom, stepTo, trade, bids, asks, multi, incompleteTrade }, orderBook } = this.props;
    return (
      <div style={{ backgroundColor: incompleteTrade ? 'tomato' : 'transparent'}}>
        <h6>
          <span style={styles.bold}>{stepFrom}</span> ({symbol}) <Badge color={marketColor(side)}>{side}</Badge>
        </h6>
        <p style={{ ...styles.target, backgroundColor: multi ? 'bisque' : '#f9f9f9'}}> Target: {trade.toFixed(8)} {stepTo}</p>
        {orderBook && <OrderBook bids={bids} asks={asks} symbol={symbol} />}
      </div>
    )
  }

}

const styles = {
  bold: {
    fontWeight: 'bold'
  },
  target: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#4b4e51',
    borderRadius: 5
  }
}

function marketColor(side) {
  return side === 'BUY' ? 'success' : 'danger'
}

export default MarketItem;
