import React from 'react';
import { Table } from 'reactstrap';

export default ({ bids, asks, symbol }) => {
  let [m2, m1] = symbol.split('/');
  if (bids && bids.length) {
    return (
      <Table style={{...styles.orderTable, ...styles.bid}}>
        <thead>
        <tr style={styles.orderRow}>
          <th>Bid ({m1})</th>
          <th>Qty ({m2})</th>
        </tr>
        </thead>
        <tbody>
        { bids.map((bid,i) => <tr key={i} style={styles.orderRow}>
          <td style={styles.td}>{bid[0]}</td>
          <td style={styles.td}>{Number(bid[1]).toFixed(3)}</td>
        </tr>)}
        </tbody>
      </Table>
    )
  } else if (asks && asks.length) {
    return (
      <Table style={{...styles.orderTable, ...styles.ask}}>
        <thead>
        <tr style={styles.orderRow}>
          <th>Ask ({m1})</th>
          <th>Qty ({m2})</th>
        </tr>
        </thead>
        <tbody>
        { asks.map((ask, i) => <tr key={i} style={styles.orderRow}>
          <td style={styles.td}>{ask[0]}</td>
          <td style={styles.td}>{Number(ask[1]).toFixed(3)}</td>
        </tr>)}
        </tbody>
      </Table>
    )
  } else return <div></div>
} ;

const styles = {
  orderTable: {
    fontSize: 13,
    background: 'transparent'
  },
  orderRow: {
    lineHeight: 0.1
  },
  td: {
    borderTop: 0
  },
  bid: {
    color: '#70a800'
  },
  ask: {
    color: '#ea0070'
  }
}
