import io from 'socket.io-client';
const socket = io();

function subscribeToBinance(cb) {
  socket.on('freshDataEmit', data => cb(null, data));
  socket.emit('subscribeToBinance', 1000);
}
export { subscribeToBinance };
