import React from 'react'
import { Provider } from 'react-redux'
import { Container } from 'reactstrap';

import './App.css'
import store from './store'

import Routes from './routes';

export default () => (
  <Provider store={store}>
    <Container style={{ marginTop: 30, textAlign: 'center' }}>
      <Routes/>
    </Container>
  </Provider>
)
