import React, { Component } from 'react';
import {connect } from 'react-redux';
import { Row, Col, Table, Badge } from 'reactstrap';
import io from 'socket.io-client';

import {getTickers} from './../store/actions/market'

const socket = io();

class Ticker extends Component {
  constructor(props) {
    super(props);
    socket.on('tickerEmit', data => this.setState({ data }));
  }

  state = {
    data: {},
    currentTime: '',
    timer: null
  };

  startTime() {
    this.setState({ currentTime: new Date().toLocaleTimeString() })
  }

  componentWillMount() {
    this.state.timer = setInterval(this.startTime.bind(this), 1000)
  }

  componentWillUnmount() {
    clearInterval(this.state.timer)
  }

  render() {
    let { data: { data, _created }, currentTime } = this.state;
    if (!data) return <div>No Data</div>
    return (
      <div>
        <h6>Last Updated: <Badge color="success">{ new Date(_created).toLocaleTimeString() }</Badge> | Current Time: <Badge color="warning">{ currentTime }</Badge></h6>
        <Row>
          <Col>
            <h4>Koinex -> BitBNS</h4>
            <Table>
              <thead>
              <tr>
                <td> </td>
                <td>Koinex</td>
                <td>BitBNS</td>
                <td>Profit (INR)</td>
              </tr>
              </thead>
              <tbody>
              { Object.keys(data).map(currency => data[currency].bitbnsSELL && <tr key={currency}>
                <td>{currency}</td>
                <td>{data[currency].koinexBUY}</td>
                <td>{data[currency].bitbnsSELL}</td>
                <td style={{ color: getColor(getPercentage(data[currency].koinexBUY, data[currency].bitbnsSELL))}}>
                  {getPercentage(data[currency].koinexBUY, data[currency].bitbnsSELL)}%
                </td>
              </tr>) }
              </tbody>
            </Table>
          </Col>
        </Row>
        <Row>
          <Col>
            <h4>BitBNS -> Koinex</h4>
            <Table>
              <thead>
              <tr>
                <td> </td>
                <td>BitBNS</td>
                <td>Koinex</td>
                <td>Profit (INR)</td>
              </tr>
              </thead>
              <tbody>
              { Object.keys(data).map(currency => data[currency].koinexSELL && <tr key={currency}>
                <td>{currency}</td>
                <td>{data[currency].bitbnsBUY}</td>
                <td>{data[currency].koinexSELL}</td>
                <td style={{ color: getColor(getPercentage(data[currency].bitbnsBUY, data[currency].koinexSELL))}}>
                  {getPercentage(data[currency].bitbnsBUY, data[currency].koinexSELL)}%
                </td>
              </tr>) }
              </tbody>
            </Table>
          </Col>
        </Row>

        {/*<Row>*/}
          {/*<Col>*/}
            {/*<h4>CEX -> BitBNS</h4>*/}
            {/*<Table>*/}
              {/*<thead>*/}
              {/*<tr>*/}
                {/*<td> </td>*/}
                {/*<td>CEX (USD)</td>*/}
                {/*<td>CEX (EUR)</td>*/}
                {/*<td>BitBNS</td>*/}
                {/*<td>Profit (USD)</td>*/}
                {/*<td>Profit (EUR)</td>*/}
              {/*</tr>*/}
              {/*</thead>*/}
              {/*<tbody>*/}
              {/*{ Object.keys(data).map(currency => data[currency].bitbnsSELL && <tr key={currency}>*/}
                {/*<td>{currency}</td>*/}
                {/*<td>{data[currency].cexUSDASK}</td>*/}
                {/*<td>{data[currency].cexEURASK}</td>*/}
                {/*<td>{data[currency].bitbnsSELL}</td>*/}
                {/*<td style={{ color: getColor(getPercentage(data[currency].cexUSDASK, data[currency].bitbnsSELL))}}>*/}
                  {/*{getPercentage(data[currency].cexUSDASK, data[currency].bitbnsSELL)}%*/}
                {/*</td>*/}
                {/*<td style={{ color: getColor(getPercentage(data[currency].cexEURASK, data[currency].bitbnsSELL))}}>*/}
                  {/*{getPercentage(data[currency].cexEURASK, data[currency].bitbnsSELL)}%*/}
                {/*</td>*/}
              {/*</tr>) }*/}
              {/*</tbody>*/}
            {/*</Table>*/}
          {/*</Col>*/}
        {/*</Row>*/}
        {/*<Row>*/}
          {/*<Col>*/}
            {/*<h4>CEX -> CoinDelta</h4>*/}
            {/*<Table>*/}
              {/*<thead>*/}
              {/*<tr>*/}
                {/*<td> </td>*/}
                {/*<td>CEX (USD)</td>*/}
                {/*<td>CEX (EUR)</td>*/}
                {/*<td>CoinDelta</td>*/}
                {/*<td>Profit (USD)</td>*/}
                {/*<td>Profit (EUR)</td>*/}
              {/*</tr>*/}
              {/*</thead>*/}
              {/*<tbody>*/}
              {/*{ Object.keys(data).map(currency => data[currency].coinDeltaBid && <tr key={currency}>*/}
                {/*<td>{currency}</td>*/}
                {/*<td>{data[currency].cexUSDASK}</td>*/}
                {/*<td>{data[currency].cexEURASK}</td>*/}
                {/*<td>{data[currency].coinDeltaBid}</td>*/}
                {/*<td style={{ color: getColor(getPercentage(data[currency].cexUSDASK, data[currency].coinDeltaBid))}}>*/}
                  {/*{getPercentage(data[currency].cexUSDASK, data[currency].coinDeltaBid)}%*/}
                {/*</td>*/}
                {/*<td style={{ color: getColor(getPercentage(data[currency].cexEURASK, data[currency].coinDeltaBid))}}>*/}
                  {/*{getPercentage(data[currency].cexEURASK, data[currency].coinDeltaBid)}%*/}
                {/*</td>*/}
              {/*</tr>) }*/}
              {/*</tbody>*/}
            {/*</Table>*/}
          {/*</Col>*/}
        {/*</Row>*/}
        {/*<Row>*/}
          {/*<Col>*/}
            {/*<h4>CEX -> Koinex</h4>*/}
            {/*<Table>*/}
              {/*<thead>*/}
              {/*<tr>*/}
                {/*<td> </td>*/}
                {/*<td>CEX (USD)</td>*/}
                {/*<td>CEX (EUR)</td>*/}
                {/*<td>Koinex</td>*/}
                {/*<td>Profit (USD)</td>*/}
                {/*<td>Profit (EUR)</td>*/}
              {/*</tr>*/}
              {/*</thead>*/}
              {/*<tbody>*/}
              {/*{ Object.keys(data).map(currency => data[currency].koinex && <tr key={currency}>*/}
                {/*<td>{currency}</td>*/}
                {/*<td>{data[currency].cexUSDASK}</td>*/}
                {/*<td>{data[currency].cexEURASK}</td>*/}
                {/*<td>{data[currency].koinex}</td>*/}
                {/*<td style={{ color: getColor(getPercentage(data[currency].cexUSDASK, data[currency].koinex))}}>*/}
                  {/*{getPercentage(data[currency].cexUSDASK, data[currency].koinex)}%*/}
                {/*</td>*/}
                {/*<td style={{ color: getColor(getPercentage(data[currency].cexEURASK, data[currency].koinex))}}>*/}
                  {/*{getPercentage(data[currency].cexEURASK, data[currency].koinex)}%*/}
                {/*</td>*/}
              {/*</tr>) }*/}
              {/*</tbody>*/}
            {/*</Table>*/}
          {/*</Col>*/}
        {/*</Row>*/}
        {/*<Row>*/}
          {/*<Col>*/}
            {/*<h4>BitBNS -> CEX</h4>*/}
            {/*<Table>*/}
              {/*<thead>*/}
              {/*<tr>*/}
                {/*<td> </td>*/}
                {/*<td>BitBNS</td>*/}
                {/*<td>CEX (USD)</td>*/}
                {/*<td>CEX (EUR)</td>*/}
                {/*<td>Profit (USD)</td>*/}
                {/*<td>Profit (EUR)</td>*/}
              {/*</tr>*/}
              {/*</thead>*/}
              {/*<tbody>*/}
              {/*{ Object.keys(data).map(currency => data[currency].bitbnsBUY && <tr key={currency}>*/}
                {/*<td>{currency}</td>*/}
                {/*<td>{data[currency].bitbnsBUY}</td>*/}
                {/*<td>{data[currency].cexUSDBID}</td>*/}
                {/*<td>{data[currency].cexEURBID}</td>*/}
                {/*<td style={{ color: getColor(getPercentage(data[currency].bitbnsBUY, data[currency].cexUSDBID))}}>*/}
                  {/*{getPercentage(data[currency].bitbnsBUY, data[currency].cexUSDBID)}%*/}
                {/*</td>*/}
                {/*<td style={{ color: getColor(getPercentage(data[currency].bitbnsBUY, data[currency].cexEURBID))}}>*/}
                  {/*{getPercentage(data[currency].bitbnsBUY, data[currency].cexEURBID)}%*/}
                {/*</td>*/}
              {/*</tr>) }*/}
              {/*</tbody>*/}
            {/*</Table>*/}
          {/*</Col>*/}
        {/*</Row>*/}
      </div>
    )
  }
}

function getPercentage(first, second) {
  return (100 -((first/second) * 100)).toFixed(2)
}

function getColor(rate) {
  return rate > 0 ? 'green' : 'red'
}

export default connect(null, { getTickers })(Ticker);
