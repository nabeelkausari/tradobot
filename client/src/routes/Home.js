import React from 'react';
import { Link } from 'react-router-dom'
import { Container, Row, Col, Button } from 'reactstrap';


export default () => (
  <Container style={{ marginTop: 30 }}>
    <Row style={{ textAlign: 'center' }}>
      <Col>
        <Button color="light">
          <Link to="/ticker">Ticker</Link>
        </Button>
      </Col>
      <Col>
        <Button color="light">
          <Link to="/bot">Bot</Link>
        </Button>
      </Col>
    </Row>
  </Container>
)
