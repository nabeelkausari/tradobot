import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from './Home';
import Bot from './Bot';
import Ticker from '../components/Arbi';

export default () => (
  <BrowserRouter>
    <Switch>
      <Route path="/" exact component={Home}/>
      <Route path="/bot" exact component={Bot}/>
      <Route path="/ticker" exact component={Ticker}/>
    </Switch>
  </BrowserRouter>
)
