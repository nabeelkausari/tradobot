import React, { Component } from 'react';
import {connect } from 'react-redux';

import { Container, Row, Col } from 'reactstrap';
import io from 'socket.io-client';
import Markets from "../components/Market/Markets"
import { updateWatchList } from '../store/actions/market'

class Bot extends Component {
  constructor(props) {
    super(props);
    const socket = io();
    socket.on('freshDataEmit', data => {
      console.log('received freshDataEmit')
      this.setState({ data: { data, updatedAt: new Date() } })
    });
  }

  state = {
    data: {
      data: [],
      updatedAt: null
    }

  };

  render() {
    return (
      <Container style={{ marginTop: 30 }}>
        <Row>
          <Col>
            <Markets updatedData={this.state.data} />
          </Col>
        </Row>
      </Container>
    )
  }
}

export default connect(null, { updateWatchList })(Bot)
