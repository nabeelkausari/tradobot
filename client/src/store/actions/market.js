import {GET_MARKETS, UPDATE_MARKETS, GET_TICKERS, GET_WATCHLIST} from './constants';

export const getMarkets = data => dispatch => {
  let queryString = data ? `?balance=${data.startingBalance}&exchange=${data.exchange.toLowerCase()}` : '';
  return fetch(`/api/opportunities${queryString}`)
    .then(res => res.json())
    .then(markets => dispatch({type: GET_MARKETS, payload: markets}))
}

export const getWatchList = data => dispatch => {
  let queryString = data ? `?exchange=${data.exchange.toLowerCase()}&marketsData=${JSON.stringify(data.marketsData)}` : '';
  return fetch(`/api/watchList${queryString}`)
    .then(res => res.json())
    .then(markets => dispatch({type: GET_WATCHLIST, payload: markets}))
    .catch(err => console.log('getWatchlistError: ', err))
}

export const getTickers = data => dispatch => {
  let queryString = data ? `?balance=${data.startingBalance}&exchange=${data.exchange.toLowerCase()}` : '';
  return fetch(`/api/tickers${queryString}`)
    .then(res => res.json())
    .then(markets => dispatch({type: GET_TICKERS, payload: markets}))
}

export const updateMarkets = (data) => dispatch => {
  return dispatch({ type: UPDATE_MARKETS, payload: data })
}

export const updateWatchList = (data) => dispatch => {
  return dispatch({ type: GET_WATCHLIST, payload: data })
}
