import axios from 'axios';
import { HOST } from "../consts";
import {
  CREATE_ORDER_A, CREATE_ORDER_A_ERROR, CREATE_ORDER_A_SUCCESS, CREATE_ORDER_B, CREATE_ORDER_B_ERROR,
  CREATE_ORDER_B_SUCCESS, CREATE_ORDER_C, CREATE_ORDER_C_ERROR, CREATE_ORDER_C_SUCCESS, GET_BALANCE, GET_BALANCE_ERROR,
  GET_BALANCE_SUCCESS, ORDER_A_COMPLETE,
  ORDER_A_PROCESSING, ORDER_B_COMPLETE, ORDER_B_PROCESSING, ORDER_C_COMPLETE, ORDER_C_PROCESSING, SELECT_ORDER
} from "./constants"


export const getBalance = ({ exchange, symbol }) => dispatch => {
  dispatch({ type: GET_BALANCE });
  axios.get(`${HOST}/api/balance?exchange=${exchange}` )
    .then(res => dispatch({ type: GET_BALANCE_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_BALANCE_ERROR, payload: err }))
}

export const selectOrder = (item) => {
  return { type: SELECT_ORDER, payload: item }
}

export const execute = ({ exchange, orderA, orderB, orderC }) => dispatch => {
  dispatch(createOrderA({ exchange, orderA, orderB, orderC}))
}

export const createOrderA = ({ exchange, orderA, orderB, orderC }) => dispatch => {
  dispatch({ type: CREATE_ORDER_A });
  axios.post(`${HOST}/api/createOrder`, { exchange, ...orderA })
    .then(res => {
      if (res.data.error) {
        dispatch({ type: CREATE_ORDER_A_ERROR, payload: res.data.error })
      } else {
        fetchData(CREATE_ORDER_A_SUCCESS, dispatch, res.data, exchange)
        dispatch(checkOrderA({ exchange, id: res.data.id, orderB, orderC }))
      }
    })
    .catch(err => dispatch({ type: CREATE_ORDER_A_ERROR, payload: err }))
}

export const checkOrderA = ({ exchange, id, orderB, orderC }) => dispatch => {
  axios.post(`${HOST}/api/fetchOrder`, { exchange, id })
    .then(res => {
      if (res.data && res.data.status === 'closed') {
        fetchData(ORDER_A_COMPLETE, dispatch, res.data, exchange)
        dispatch(createOrderB({ exchange, orderB, orderC}))
      } else {
        dispatch({ type: ORDER_A_PROCESSING, payload: res.data });
        dispatch(checkOrderA({ exchange, id, orderB, orderC }))
      }
    })
    .catch(err => dispatch({ type: CREATE_ORDER_A_ERROR, payload: err }))
}

// symbol, type, side, amount, price

export const createOrderB = ({ exchange, orderB, orderC }) => dispatch => {
  dispatch({ type: CREATE_ORDER_B });
  axios.post(`${HOST}/api/createOrder`, { exchange, ...orderB })
    .then(res => {
      if (res.data.error) {
        dispatch({ type: CREATE_ORDER_B_ERROR, payload: res.data.error })
      } else {
        fetchData(CREATE_ORDER_B_SUCCESS, dispatch, res.data, exchange)
        dispatch(checkOrderB({ exchange, id: res.data.id, orderC }))
      }
    })
    .catch(err => dispatch({ type: CREATE_ORDER_B_ERROR, payload: err }))
}

export const checkOrderB = ({ exchange, id, orderC }) => dispatch => {
  axios.post(`${HOST}/api/fetchOrder`, { exchange, id })
    .then(res => {
      if (res.data && res.data.status === 'closed') {
        fetchData(ORDER_B_COMPLETE, dispatch, res.data, exchange);
        dispatch(createOrderC({ exchange, orderC }))
      } else {
        dispatch({ type: ORDER_B_PROCESSING, payload: res.data })
        dispatch(checkOrderB({ exchange, id, orderC }))
      }
    })
    .catch(err => dispatch({ type: CREATE_ORDER_B_ERROR, payload: err }))
}

export const createOrderC = ({ exchange, orderC }) => dispatch => {
  dispatch({ type: CREATE_ORDER_C });
  // ORDER: symbol, type, side, amount, price
  axios.post(`${HOST}/api/createOrder`, { exchange, ...orderC })
    .then(res => {
      if (res.data.error) {
        dispatch({ type: CREATE_ORDER_C_ERROR, payload: res.data.error })
      } else {
        fetchData(CREATE_ORDER_C_SUCCESS, dispatch, res.data, exchange)
        dispatch(checkOrderC({ exchange, id: res.data.id }))
      }
    })
    .catch(err => dispatch({ type: CREATE_ORDER_C_ERROR, payload: err }))
}

export const checkOrderC = ({ exchange, id }) => dispatch => {
  axios.post(`${HOST}/api/fetchOrder`, { exchange, id })
    .then(res => {
      if (res.data && res.data.status === 'closed') {
        fetchData(ORDER_C_COMPLETE, dispatch, res.data, exchange)
      } else {
        dispatch({ type: ORDER_C_PROCESSING, payload: res.data })
        dispatch(checkOrderC({ exchange, id }))
      }
    })
    .catch(err => dispatch({ type: CREATE_ORDER_C_ERROR, payload: err }))
}

const fetchData = (type, dispatch, payload, exchange) => {
  dispatch({ type, payload })
  dispatch(getBalance({ exchange }));
}
