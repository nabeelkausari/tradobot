import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger'
import rootReducer from './reducers';
import { GET_WATCHLIST, GET_MARKETS } from "./actions/constants"

const logger = createLogger({
  predicate: (getState, action) => action.type !==  GET_MARKETS && action.type !== GET_WATCHLIST
})

const middleware = [
  thunk,
  logger
]
const withDevTools = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(rootReducer, withDevTools(
  applyMiddleware(...middleware)
))
