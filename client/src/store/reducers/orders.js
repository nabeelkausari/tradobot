import {
  CREATE_ORDER_A, CREATE_ORDER_A_ERROR, CREATE_ORDER_A_SUCCESS, CREATE_ORDER_B, CREATE_ORDER_B_ERROR,
  CREATE_ORDER_B_SUCCESS, CREATE_ORDER_C, CREATE_ORDER_C_ERROR, CREATE_ORDER_C_SUCCESS, GET_BALANCE, GET_BALANCE_ERROR,
  GET_BALANCE_SUCCESS, ORDER_A_COMPLETE,
  ORDER_A_PROCESSING, ORDER_B_COMPLETE, ORDER_B_PROCESSING, ORDER_C_COMPLETE, ORDER_C_PROCESSING, SELECT_ORDER
} from '../actions/constants'

const orderReducer = (state = {}, {type, payload}) => {
  switch (type) {
    case GET_BALANCE:
      return { ...state, balanceLoading: true};
    case GET_BALANCE_SUCCESS:
      return { ...state, balanceLoading: false, balances: payload };
    case GET_BALANCE_ERROR:
      return { ...state, balanceLoading: false, balanceErr: payload };

    case SELECT_ORDER:
      return { ...state, selected: payload};

    case CREATE_ORDER_A:
      return { ...state, createALoading: true, orderAProcessing: true, orderAComplete: false, createAErr: null };
    case CREATE_ORDER_A_SUCCESS:
      return { ...state, createALoading: false, orderA: payload };
    case CREATE_ORDER_A_ERROR:
      return { ...state, createALoading: false, createAErr: payload };
    case ORDER_A_PROCESSING:
      return { ...state, orderAProcessing: true };
    case ORDER_A_COMPLETE:
      return { ...state, orderAProcessing: false, orderAComplete: true };

    case CREATE_ORDER_B:
      return { ...state, createBLoading: true, orderBProcessing: true, orderBComplete: false, createBErr: null };
    case CREATE_ORDER_B_SUCCESS:
      return { ...state, createBLoading: false, orderB: payload };
    case CREATE_ORDER_B_ERROR:
      return { ...state, createBLoading: false, createBErr: payload };
    case ORDER_B_PROCESSING:
      return { ...state, orderBProcessing: true };
    case ORDER_B_COMPLETE:
      return { ...state, orderBProcessing: false, orderBComplete: true };

    case CREATE_ORDER_C:
      return { ...state, createCLoading: true, orderCProcessing: true, orderCComplete: false, createCErr: null };
    case CREATE_ORDER_C_SUCCESS:
      return { ...state, createCLoading: false, orderC: payload };
    case CREATE_ORDER_C_ERROR:
      return { ...state, createCLoading: false, createCErr: payload };
    case ORDER_C_PROCESSING:
      return { ...state, orderCProcessing: true };
    case ORDER_C_COMPLETE:
      return { ...state, orderCProcessing: false, orderCComplete: true };

    default:
      return state
  }
}

export { orderReducer };
