import {combineReducers} from 'redux';
import customerReducer from './customer';
import { marketReducer, watchListReducer } from './market';
import { orderReducer } from './orders';

export default combineReducers({
  customers: customerReducer,
  markets: marketReducer,
  watchListMarkets: watchListReducer,
  orders: orderReducer
})
