import {GET_MARKETS, UPDATE_MARKETS, GET_TICKERS, GET_WATCHLIST} from '../actions/constants'

const marketReducer = (state = [], {type, payload}) => {
  switch (type) {
    case GET_MARKETS:
    case UPDATE_MARKETS:
      if (payload.error) return state
      return payload
    case GET_TICKERS:
      return state
    default:
      return state
  }
}
const watchListReducer = (state = [], {type, payload}) => {
  switch (type) {
    case GET_WATCHLIST:
      if (payload.error) return state
      return payload
    default:
      return state
  }
}

export { marketReducer, watchListReducer };
